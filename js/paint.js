var Modes = {

    //draw
    line_naive: "drawLineNaive",
    line_bresenham: "drawLineBresenham",
    line_bresenham_int: "drawLineBresenhamInteger",

    //ellipse

    ellipse_naive : "drawEllipseNaive",
    ellipse_bresenham : "drawEllipseBresenham",

    //fill
    fill : "fillArea",

    //cut
    cut : "cropArea",

    //rotate
    rotate : "rotate",
    rotateOwen : "rotateOwen",
    drag : "drag"

};

//region Painter

function SimpleLinePainter(el, unit) {
    this.unit = unit || 9;
    this.el = el;

    this.width = 900;
    this.height = 480;

    this.startXY = {x: 0, y: 0};
    this.endXY = {x: 0, y: 0};
    this.mode = Modes.line_naive;
    this.context = el.getContext("2d");

    this.sizeX = Math.floor(this.width/this.unit);
    this.sizeY = Math.floor(this.height/this.unit);

    this.defaultColor = '#ffffff';
    this.pixels = this.getClean();

    this.color = '#000000';

    var t = this;
    this.el.onmousedown = function (e) {
        t.startXY = t.getXY(e);
    };
    this.el.onmouseup = function (e) {
        t.endXY = t.getXY(e);
        t.process();
    };

    this.drawGrid();
}

//region Grid

SimpleLinePainter.prototype.drawGrid = function (color) {
    color = color || '#cccccc';
    this.setColor(color);
    this.context.lineWidth = 1;
    var offsetY = this.unit;
    while (offsetY < this.height) {
        this.context.beginPath();
        this.context.moveTo(0, offsetY);
        this.context.lineTo(this.width, offsetY);
        this.context.stroke();
        offsetY += this.unit;
    }
    var offsetX = this.unit;
    while (offsetX < this.width) {
        this.context.beginPath();
        this.context.moveTo(offsetX, 0);
        this.context.lineTo(offsetX, this.height);
        this.context.stroke();
        offsetX += this.unit;
    }

};

//endregion

//region Basic Tweaking and Helpers

SimpleLinePainter.prototype.perform = function (x1, y1, x2, y2) {
    this.setStartEnd(x1, y1, x2, y2);
    //console.log(this.mode);
    this.process();
};

SimpleLinePainter.prototype.process = function () {
    if (this[this.mode] !== 'undefined') {
        this[this.mode]();
    }
};

SimpleLinePainter.prototype.setColor = function (color) {
    if(!color.match(/#[\da-f]{6}/)){
        alert('Invalid color!');
        return;
    }
    this.context.strokeStyle = color;
    this.context.fillStyle = color;
    this.color = color;
};

SimpleLinePainter.prototype.setMode = function (mode) {
    if (Modes[mode]) {
        this.mode = Modes[mode];
    } else {
        alert('Invalid mode!');
    }
};

SimpleLinePainter.prototype.getXY = function (e) {
    return {
        x: Math.floor((e.pageX - this.el.offsetLeft)/this.unit) * this.unit,
        y: Math.floor((e.pageY - this.el.offsetTop)/this.unit) * this.unit
    };
};

SimpleLinePainter.prototype.getDelta = function () {
    return {x: this.endXY.x - this.startXY.x, y: this.endXY.y - this.startXY.y}
};

SimpleLinePainter.prototype.swapEndPoints = function () {
    var t = this.endXY;
    this.endXY = this.startXY;
    this.startXY = t;
};

SimpleLinePainter.prototype.setStartEnd = function(x1, y1, x2, y2){
    this.startXY.x = x1;
    this.startXY.y = y1;
    this.endXY.x = x2;
    this.endXY.y = y2;
};

SimpleLinePainter.prototype.fillPx = function (x, y) {
    //this.context.fillRect(x, y, this.unit, this.unit);
    this.fillPxStretched(x/this.unit, y/this.unit);
};

SimpleLinePainter.prototype.getPx = function(x, y){
    return this.pixels[x][y] || this.defaultColor;
};

SimpleLinePainter.prototype.saveColor = function(){
    this.savedColor = this.color;
};

SimpleLinePainter.prototype.restoreColor = function(){
    this.setColor(this.savedColor);
};

SimpleLinePainter.prototype.setPx = function(x, y){
    this.pixels[x][y] = this.color;
};

SimpleLinePainter.prototype.fillPxStretched = function (x, y) {
    x = Math.floor(x);
    y = Math.floor(y);
    //console.log("filling " + x + ' ' + y);
    this.setPx(x, y);
    this.context.fillRect(this.unit * x + 1, this.unit * y + 1, this.unit - 2, this.unit - 2);
};
//endregion

//region Draw Line

//SimpleLinePainter.prototype.drawLine = function () {
//    this.context.beginPath();
//    this.context.moveTo(this.startXY.x, this.startXY.y);
//    this.context.lineTo(this.endXY.x, this.endXY.y);
//    this.context.stroke();
//};

SimpleLinePainter.prototype.drawLineNaive = function () {
    var delta = this.getDelta();
    var coefficient = delta.y / delta.x;
    var x = this.startXY.x, y = this.startXY.y;
    if (!isNaN(coefficient) && Math.abs(coefficient) <= 1) {
        if (delta.x < 0) {
            this.swapEndPoints();
            return this.drawLineNaive();
        }
        while (x <= this.endXY.x) {
            y = Math.floor((this.startXY.y + (x - this.startXY.x) * coefficient) / this.unit) * this.unit;
            this.fillPx(x, y);
            x += this.unit;
        }
    } else {
        coefficient = delta.x / delta.y;
        if (delta.y < 0) {
            this.swapEndPoints();
            return this.drawLineNaive();
        }
        while (y <= this.endXY.y) {
            x = Math.floor((this.startXY.x + (y - this.startXY.y) * coefficient) / this.unit) * this.unit;
            this.fillPx(x, y);
            y += this.unit;
        }
    }


};

SimpleLinePainter.prototype.stretchAss = function(){
    var unit = this.unit;
    var stretchFunction = function (obj) {
        return Math.round(obj / unit);
    };
    this.startXY.x = stretchFunction(this.startXY.x);
    this.startXY.y = stretchFunction(this.startXY.y);
    this.endXY.x = stretchFunction(this.endXY.x);
    this.endXY.y = stretchFunction(this.endXY.y);
};

SimpleLinePainter.prototype.drawLineBresenham = function (needNotToStretch) {
    if(!needNotToStretch)
        this.stretchAss();
    var delta = this.getDelta();
    var t = this;
    var error = -0.5;
    var deltaError = Math.abs(delta.y * 1. / delta.x);
    var unit = 1;

    var changeY = function () {
        return (delta.y > 0) ? unit : -unit;
    };
    var changeX = function () {
        return (delta.x > 0) ? unit : -unit;
    };

    var x = this.startXY.x;
    var y = this.startXY.y;

    //deltaError
    var performDrawingAction = function (lambda) {
        t.fillPxStretched(x, y);
        error += deltaError;
        if (error >= 0.5) {
            lambda();
            error -= 1;
        }
    };

    if (deltaError <= 1) {
        if (delta.x < 0) {
            this.swapEndPoints();
            return this.drawLineBresenham(true);
        }
        while (x <= this.endXY.x) {
            performDrawingAction(function () {
                y += changeY();
            });
            x += unit;
        }
    } else {
        if (delta.y < 0) {
            this.swapEndPoints();
            return this.drawLineBresenham(true);
        }
        deltaError = 1 / deltaError;
        while (y <= this.endXY.y) {
            performDrawingAction(function () {
                x += changeX();
            });
            y += unit;
        }
    }

};

SimpleLinePainter.prototype.drawLineBresenhamInteger = function (needNotToStretch) {
    if(!needNotToStretch)
        this.stretchAss();
    var delta = this.getDelta();
    var t  = this;
    var error = -delta.x;
    var deltaError = Math.abs(2 * delta.y);
    var baseErrorDifference = Math.abs(delta.x);
    var unit = 1;

    var changeY = function () {
        return (delta.y > 0) ? unit : -unit;
    };
    var changeX = function () {
        return (delta.x > 0) ? unit : -unit;
    };

    var x = this.startXY.x;
    var y = this.startXY.y;


    //deltaError
    var performDrawingAction = function (lambda) {
        t.fillPxStretched(x, y);
        error += deltaError;
        if (error >= baseErrorDifference) {
            lambda();
            error -= 2 * baseErrorDifference;
        }
    };

    if (deltaError <= 2 * baseErrorDifference) {

        if (delta.x < 0) {
            this.swapEndPoints();
            return this.drawLineBresenhamInteger(true);
        }
        while (x <= this.endXY.x) {
            performDrawingAction(function () {
                y += changeY();
            });
            x += unit;
        }
    } else {
        if (delta.y < 0) {
            this.swapEndPoints();
            return this.drawLineBresenhamInteger(true);
        }
        error = -delta.y;
        baseErrorDifference = Math.abs(delta.y);
        deltaError = Math.abs(2 * delta.x);
        while (y <= this.endXY.y) {
            performDrawingAction(function () {
                x += changeX();
            });
            y += unit;
        }
    }

};

//endregion

//region Ellipse

SimpleLinePainter.prototype.drawEllipseBresenham = function(){
    this.drawEllipseBresenhamFromCenter(
        0.5*(this.startXY.x + this.endXY.x),
        0.5*(this.startXY.y + this.endXY.y),
        Math.abs(0.5*(this.startXY.x - this.endXY.x)),
        Math.abs(0.5*(this.startXY.y - this.endXY.y))
    );
};

SimpleLinePainter.prototype.drawEllipseNaive = function(){
    this.drawEllipseNaiveFromCenter(
        0.5*(this.startXY.x + this.endXY.x),
        0.5*(this.startXY.y + this.endXY.y),
        Math.abs(0.5*(this.startXY.x - this.endXY.x)),
        Math.abs(0.5*(this.startXY.y - this.endXY.y))
    );
};

SimpleLinePainter.prototype.drawEllipseNaiveFromCenter = function(x0, y0, width, height){
    x0 /= this.unit;
    y0 /= this.unit;
    height /= this.unit;
    width /= this.unit;

    var x, y, thisObj = this, i;

    var dwarPix = function () {
        thisObj.fillPxStretched(x0 + x, y0 + y);
        thisObj.fillPxStretched(x0 - x, y0 + y);
        thisObj.fillPxStretched(x0 + x, y0 - y);
        thisObj.fillPxStretched(x0 - x, y0 - y);
    };

    for (i = 0; i <= width; i++) {
        x = i;
        y = Math.floor(Math.sqrt((1 - (i * i) / (width * width)) * height * height));
        dwarPix();
    }
    for (i = 0; i <= height; i++) {
        y = i;
        x = Math.floor(Math.sqrt((1 - (i * i) / (height * height)) * width * width));
        dwarPix();
    }
};

SimpleLinePainter.prototype.drawEllipseBresenhamFromCenter = function(x0, y0, width, height){

        if(height === 0 || width === 0)
            return;

        x0 /= this.unit;
        y0 /= this.unit;
        height /= this.unit;
        width /= this.unit;

        var radiusByXSquared = width * width;
        var radiusByYSquared = height * height;
        var radiusByXSquaredQuadrupled = 4 * radiusByXSquared, radiusByYSquaredQuardrupled = 4 * radiusByYSquared;
        var x, y, sigma;

        //    o         o
        //  o             o
        //  o             o
        //    o         o
        for (x = 0, y = height, sigma = 2*radiusByYSquared+radiusByXSquared*(1-2*height); radiusByYSquared*x <= radiusByXSquared*y; x++)
        {
            this.fillPxStretched(x0 + x, y0 + y);
            this.fillPxStretched(x0 - x, y0 + y);
            this.fillPxStretched(x0 + x, y0 - y);
            this.fillPxStretched(x0 - x, y0 - y);
            if (sigma >= 0)
            {
                sigma += radiusByXSquaredQuadrupled * (1 - y);
                y--;
            }
            sigma += radiusByYSquared * ((4 * x) + 6);
        }

        //    ooooo
        //  oo     oo
        //
        //  oo     oo
        //    ooooo
        for (x = width, y = 0, sigma = 2*radiusByXSquared+radiusByYSquared*(1-2*width); radiusByXSquared*y <= radiusByYSquared*x; y++)
        {
            this.fillPxStretched(x0 + x, y0 + y);
            this.fillPxStretched(x0 - x, y0 + y);
            this.fillPxStretched(x0 + x, y0 - y);
            this.fillPxStretched(x0 - x, y0 - y);
            if (sigma >= 0)
            {
                sigma += radiusByYSquaredQuardrupled * (1 - x);
                x--;
            }
            sigma += radiusByXSquared * ((4 * y) + 6);
        }

};

//endregion

SimpleLinePainter.prototype.repaint = function () {
    this.saveColor();
    this.setColor(this.defaultColor);
    //this.context.fillRect(0,0,this.width, this.height);
    for(var i = 0; i < Math.floor(this.width/this.unit); ++i) {
        for (var j = 0; j < Math.floor(this.height / this.unit); ++j) {
            this.setColor(this.pixels[i][j]);
            this.fillPxStretched(i, j);
        }
    }
    this.restoreColor();
};

SimpleLinePainter.prototype.getClean = function () {
    var clean = [];
    for(var i = 0; i < Math.floor(this.width/this.unit); ++i) {
        clean.push([]);
        for (var j = 0; j < Math.floor(this.height / this.unit); ++j) {
            clean[i].push(this.defaultColor);
        }
    }
    return clean;
};

SimpleLinePainter.prototype.doRotationOnButtonClick = function () {
    var deg = Number($('#rotate-deg').val()) * Math.PI / 180;
    if(this.mode.indexOf('rotate') != -1){
        this[this.mode](deg);
    }
};

SimpleLinePainter.prototype.rotateOwen = function (alpha) {
    if(!alpha)
        return;
    var newCanvas = this.getClean();
    var delta = this.getDelta();
    var cos = Math.cos(alpha);
    var sin = Math.sin(alpha);
    var tan = Math.tan(alpha/2);
    this.stretchAss();
    for(var i = 0; i < this.sizeX; ++i) {
        for (var j = 0; j < this.sizeY; ++j) {
            if(this.defaultColor != this.pixels[i][j]){
                var newI = Math.floor(i - (j - this.startXY.y) * tan),
                    newJ = j;
                    //newI = newI;
                    newJ = Math.floor(sin * (newI - this.startXY.x) + newJ);
                    newI = Math.floor(newI - tan * (newJ - this.startXY.y));
                    //newJ = newJ;
                if(0 <= newI && newI < this.sizeX && 0 <= newJ && newJ < this.sizeY){
                    //console.log({old_i : i, old_j : j, i: newI, j: newJ});
                    newCanvas[newI][newJ] = this.pixels[i][j];
                    //this.fillPxStretched(newI, newJ);
                }
            }
        }
    }
    //console.log(newCanvas);
    this.pixels = newCanvas;
    this.repaint();
};
SimpleLinePainter.prototype.rotate = function (alpha) {
    if(!alpha)
        return;
    var newCanvas = this.getClean();
    var delta = this.getDelta();
    var cos = Math.cos(alpha);
    var sin = Math.sin(alpha);
    this.stretchAss();
    for(var i = 0; i < this.sizeX; ++i) {
        for (var j = 0; j < this.sizeY; ++j) {
            if(this.defaultColor != this.pixels[i][j]){
                var newI = Math.round((i - this.startXY.x) * cos + (j - this.startXY.y) * sin) + this.startXY.x,
                    newJ = Math.round((i - this.startXY.x) * sin - (j - this.startXY.y) * cos) + this.startXY.y;
                if(0 <= newI && newI < this.sizeX && 0 <= newJ && newJ < this.sizeY){
                    //console.log({old_i : i, old_j : j, i: newI, j: newJ});
                    newCanvas[newI][newJ] = this.pixels[i][j];
                    //this.fillPxStretched(newI, newJ);
                }
            }
        }
    }
    //console.log(newCanvas);
    this.pixels = newCanvas;
    this.repaint();
};

SimpleLinePainter.prototype.drag = function () {
    var newCanvas = this.getClean();
    this.stretchAss();
    var delta = this.getDelta();
    for(var i = 0; i < this.sizeX; ++i) {
        for (var j = 0; j < this.sizeY; ++j) {
            if(this.defaultColor != this.pixels[i][j]){
                var newI = i + delta.x,
                    newJ = j + delta.y;
                if(0 <= newI && newI < this.sizeX && 0 <= newJ && newJ < this.sizeY){
                    //console.log({old_i : i, old_j : j, i: newI, j: newJ});
                    newCanvas[newI][newJ] = this.pixels[i][j];
                    //this.fillPxStretched(newI, newJ);
                }
            }
        }
    }
    //console.log(newCanvas);
    this.pixels = newCanvas;
    this.repaint();
};

SimpleLinePainter.prototype.fillArea = function(){
    var x = Math.floor(this.endXY.x / this.unit);
    var y = Math.floor(this.endXY.y / this.unit);
    var colorToFill = this.getPx(x, y);

    if(colorToFill == this.color)
        return;
    var t = this;

    var points = [];
    var checkIfOkToVisitAndVisitIfOk = function(x, y){
        if(x < t.pixels.length && x >= 0 && y < t.pixels[0].length && y >= 0){
            points.push([x, y]);
        }
    };
    var visitPoint = function(x, y){
        //console.log(t.pixels[x][y] + " - "  + colorToFill);
        if(t.getPx(x, y) === colorToFill){
            //console.log(x + " " + y);
            t.fillPxStretched(x, y);
            //return;
            checkIfOkToVisitAndVisitIfOk(x - 1, y);
            checkIfOkToVisitAndVisitIfOk(x + 1, y);
            checkIfOkToVisitAndVisitIfOk(x, y + 1);
            checkIfOkToVisitAndVisitIfOk(x, y - 1);
        }
    };

    visitPoint(x, y);
    var currentPoint;
    while(currentPoint = points.pop()){
        visitPoint(currentPoint[0], currentPoint[1]);
    }
};

SimpleLinePainter.prototype.round = function(el){
    return Math.floor(el/this.unit) * this.unit;
};

SimpleLinePainter.prototype.roundPoints = function(){
    this.endXY.x = this.round(this.endXY.x);
    this.endXY.y = this.round(this.endXY.y);
    this.startXY.x = this.round(this.startXY.x);
    this.startXY.y = this.round(this.startXY.y);
};

SimpleLinePainter.prototype.cropArea = function(){
    this.roundPoints();
    this.setColor(this.defaultColor);
    var t = 0;
    if(this.startXY.x > this.endXY.x){
        t = this.startXY.x;
        this.startXY.x = this.endXY.x;
        this.endXY.x = t;
    }
    if(this.startXY.y > this.endXY.y){
        t = this.startXY.y;
        this.startXY.y = this.endXY.y;
        this.endXY.y = t;
    }

    for(var x = 0; x < this.pixels.length; ++x){
        for(var y = 0; y < this.pixels[0].length; ++y){
            console.log('sss');
            if(
            !(
            x > this.startXY.x/this.unit && x < this.endXY.x/this.unit && y > this.startXY.y/this.unit && y < this.endXY.y/this.unit
            )
            && this.getPx(x, y) != this.defaultColor
            )
                this.fillPxStretched(x, y);
        }
    }
    //this.context.fillRect(0, 0, this.startXY.x, this.height);
    //this.context.fillRect(0, 0, this.width, this.startXY.y);
    //this.context.fillRect(0, this.endXY.y, this.width, this.height - this.endXY.y);
    //this.context.fillRect(this.endXY.x, 0, this.width - this.endXY.x, this.height);
    this.setColor(this.color);
};

//endregion


var painter = new SimpleLinePainter(document.getElementsByTagName('canvas')[0]);

function draw(x1, y1, x2, y2, mode, color) {
    painter.setColor(color);
    painter.startXY = {x: x1, y: y1};
    painter.endXY = {x: x2, y: y2};
    painter.setMode(mode);
    painter.process();
}

//region Tests and stuff

//draw(200, 200, 250, 400,"bresenham", '#cc00ff'); //good
//draw(200, 200, 150, 400,"bresenham", '#1100ff'); //good
//draw(200, 200, 250,   0,"bresenham", '#cf0fff'); //good
//draw(200, 200, 150,   0,"bresenham", '#1f0fff'); //good
//draw(200, 200,   0, 150,"bresenham", '#cf0f2f'); //good
//draw(200, 200, 400, 150,"bresenham", '#cf0f2f'); //good
//draw(200, 200,   0, 250,"bresenham", '#cf9f2f'); //good
//draw(200, 200, 400, 250,"bresenham", '#cf9f2f'); //good

function testLine(mode, color) {
    draw(200, 200, 250, 400, mode, color); //good
    draw(200, 200, 150, 400, mode, color); //good
    draw(200, 200, 250, 0, mode, color); //good
    draw(200, 200, 150, 0, mode, color); //good
    draw(200, 200, 0, 150, mode, color); //good
    draw(200, 200, 400, 150, mode, color); //good
    draw(200, 200, 0, 250, mode, color); //good
    draw(200, 200, 400, 250, mode, color); //good
}

painter.setColor('#000000');
//painter.unit = 1;
//draw(200, 200, 600, 400, "ellipse_bresenham", "black");
//draw(200, 200, 600, 400, "ellipse_naive", "red");


testLine('line_bresenham', '#9f0f3f');
//painter.rotate(0.5);
//testLine('line_bresenham_int', '#ff0000');

//endregion